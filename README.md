# raspberry-env

My Raspberry Pi Deployment Stuff

# Steps to deploy

1. Use dd or etcher or similar program to copy raspbian image to micro SD
2. mount boot partition with `sudo mount -t msdos /dev/diskN/s1 mountdir` (Change
accordingly for non-Mac)
3. Copy a valid `wpa_supplicant.conf` file to mountdir
4. Do `echo 'ssh'|sudo tee mountdir/ssh`
5. Unmount boot partition
6. Eject disk (in my case it was `diskutil eject /dev/disk2`)
7. Boot raspberry pi with micro SD card, it should connect to WiFi and have ssh
enabled.
8. Add the ip to Ansible inventory `inventory/hosts.inv`
9. Run `ansible-playbook playbooks/rpi_setup.yml -k` at password prompt enter
'raspberry'

